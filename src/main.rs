#![forbid(unsafe_code)]
// clap
use clap::{Parser, Subcommand, ValueEnum};

use num::complex::Complex64;
use num::Complex;
use ractal::{run,save_fractal};
use std::str::FromStr;

#[derive(Parser)]
#[command(version, about, long_about = None)]

struct Cli {
    /// filename to save image to (--no-ui or press enter)
    #[arg(short, long, value_name = "FILENAME", default_value = "mandel.png")]
    filename: String,

    /// no user interface, draw to file and exits. max image size depends on your available RAM
    #[arg(short, long, value_name = "NO-UI")]
    no_ui: bool,

    /// size in pixels of the window
    #[arg(short, long, value_name = "SIZEX_x_SIZEY", default_value = "750x750")]
    size: String,

    /// upper_left corner of the window in the complex plane
    #[arg(
        short,
        long,
        value_name = "UPPER_LEFT_CORNER",
        default_value = "(-1.0,1.0)"
    )]
    upper_left: String,

    /// upper_left corner of the window in the complex plane
    #[arg(
        short,
        long,
        value_name = "LOWER_RIGHT_CORNER",
        default_value = "(1,-1)"
    )]
    lower_right: String,

    /// define the 'c' parameter of the julia set - ignored for mandelbrot
    #[arg(short, long, value_name = "c", default_value = "(-0.8,0.156)")]
    c: String,
    //TODO subcommands for each render, with args (ITER, ...)
    /// fractal type:
    #[arg(short, long, value_name = "TYPE", default_value = "julia")]
    type_of_fractal: ftypes,
    //TODO subcommands for each render, with args (ITER, ...)
}

/// possible value for cli arg fractal_types
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum ftypes {
    // Julia set, classic coloring (exit speed)
    julia,
    // Mandelbrot set, classic coloring (exit speed)
    mandelbrot,
    // Mandelbrot set, normal map coloring
    mandelbrot_normal,
}

fn main() {
    env_logger::init();
    let cli = Cli::parse();

    let bounds = parse_pair(cli.size, 'x').expect("error parsing image dimensions");
    let upper_left = parse_complex(cli.upper_left).expect("error parsing upper left corner point");
    let lower_right =
        parse_complex(cli.lower_right).expect("error parsing lower right corner point");
    let c = parse_complex(cli.c).expect("error parsing c");

    let ft = match cli.type_of_fractal {
        ftypes::julia => "julia",
        ftypes::mandelbrot => "mandelbrot",
        ftypes::mandelbrot_normal => "mandelbrot_normal",
    };

    log::info!(
        "starting {:?} with parameters: pixels:{} by {}  complex plane: ┏ = {}  ┛ = {}   Julia's C = {}",
        ft,
        bounds.0,
        bounds.1,
        upper_left,
        lower_right,
        c
    );

    if cli.no_ui {
        save_fractal(
            cli.filename,
            ft.to_string(),
            bounds,
            upper_left,
            lower_right,
            c,
        )
        .expect("bug, sorry")
    } else {
        run(
            cli.filename,
            ft.to_string(),
            bounds,
            upper_left,
            lower_right,
            c,
        )
        .expect("bug, sorry")
    }
}

/// Parse the string `s` as a coordinate pair, like `"(1.0,0.5)"`.
///
/// Specifically, `s` should have the form (<left><sep><right>), where <sep> is
/// the character given by the `separator` argument, and <left> and <right> are both
/// strings that can be parsed by `T::from_str`.
///
/// If `s` has the proper form, return `Some<(x, y)>`. If it doesn't parse
/// correctly, return `None`.
fn parse_pair<T: FromStr>(mut s: String, separator: char) -> Option<(T, T)> {
    s.retain(|c| c != '(' && c != ')');
    match s.find(separator) {
        None => None,
        Some(index) => match (T::from_str(&s[..index]), T::from_str(&s[index + 1..])) {
            (Ok(l), Ok(r)) => Some((l, r)),
            _ => None,
        },
    }
}

#[test]
fn test_parse_pair() {
    assert_eq!(parse_pair::<i32>("", ','), None);
    assert_eq!(parse_pair::<i32>("10,", ','), None);
    assert_eq!(parse_pair::<i32>(",10", ','), None);
    assert_eq!(parse_pair::<i32>("10,20", ','), Some((10, 20)));
    assert_eq!(parse_pair::<i32>("10,20xy", ','), None);
    assert_eq!(parse_pair::<f64>("0.5x", 'x'), None);
    assert_eq!(parse_pair::<f64>("0.5x1.5", 'x'), Some((0.5, 1.5)));
}

/// Parse a pair of floating-point numbers separated by a comma as a complex
/// number.
pub fn parse_complex(s: String) -> Option<Complex<f64>> {
    match parse_pair(s, ',') {
        Some((re, im)) => Some(Complex64 { re, im }),
        None => None,
    }
}

#[test]
fn test_parse_complex() {
    assert_eq!(
        parse_complex("1.25,-0.0625"),
        Some(Complex64 {
            re: 1.25,
            im: -0.0625
        })
    );
    assert_eq!(parse_complex(",-0.0625"), None);
}
