// dark
const SEUIL1: f64 = 0.30;
// yellow
const SEUIL2: f64 = 0.40;
// bright red
const SEUIL3: f64 = 0.50;

pub fn mix(c1: [u8; 3], c2: [u8; 3], mix: f64) -> [u8; 3] {
    //Invert sRGB gamma compression
    let c1 = inverse_srgb_companding(c1);
    let c2 = inverse_srgb_companding(c2);

    let r = c1[0] as f64 * (1.0 - mix) + c2[0] as f64 * (mix);
    let g = c1[1] as f64 * (1.0 - mix) + c2[1] as f64 * (mix);
    let b = c1[2] as f64 * (1.0 - mix) + c2[2] as f64 * (mix);

    //Reapply sRGB gamma compression
    srgb_companding([r as u8, g as u8, b as u8])
    //[r as u8, g as u8, b as u8]
}

fn srgb_companding(c: [u8; 3]) -> [u8; 3] {
    //Convert color from 0..255 to 0..1
    let mut r = c[0] as f64 / 255.0;
    let mut g = c[1] as f64 / 255.0;
    let mut b = c[2] as f64 / 255.0;

    //Apply companding to Red, Green, and Blue
    if r > 0.0031308 {
        r = 1.055 * r.powf(1.0 / 2.4) - 0.055
    } else {
        r = r * 12.92;
    };
    if g > 0.0031308 {
        g = 1.055 * g.powf(1.0 / 2.4) - 0.055
    } else {
        g = g * 12.92;
    };
    if b > 0.0031308 {
        b = 1.055 * b.powf(1.0 / 2.4) - 0.055
    } else {
        b = b * 12.92;
    };

    //return new color. Convert 0..1 back into 0..255
    [(r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8]
}

fn inverse_srgb_companding(c: [u8; 3]) -> [u8; 3] {
    //Convert color from 0..255 to 0..1
    let mut r = c[0] as f64 / 255.0;
    let mut g = c[1] as f64 / 255.0;
    let mut b = c[2] as f64 / 255.0;

    //Inverse Red, Green, and Blue
    if r > 0.04045 {
        r = ((r + 0.055) / 1.055).powf(2.4)
    } else {
        r = r / 12.92;
    };
    if g > 0.04045 {
        g = ((g + 0.055) / 1.055).powf(2.4)
    } else {
        g = g / 12.92;
    };
    if b > 0.04045 {
        b = ((b + 0.055) / 1.055).powf(2.4)
    } else {
        b = b / 12.92;
    };

    //return new color. Convert 0..1 back into 0..255

    [(r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8]
}

/// Create a palette 256 color palette
///
/// * the palette is made of 5 colors that are hard coded
/// * the position of each color is defined by the 3 const SEUILx
/// * smooth transition betwen colors
pub fn create_palette() -> Vec<[u8; 3]> {
    let grey = [180, 212, 229];
    let dark = [40, 48, 64]; //SEUIL1
    let yellow = [192, 192, 18]; //SEUIL2
    let orange = [192, 25, 25]; //SEUIL3
    let red = [64, 0, 0];

    let mut palette = vec![[0, 0, 0]; 256];

    for i in 0..255 {
        let m = i as f64 / 255.0;
        palette[i] = if m < SEUIL1 {
            mix(grey, dark, (m - 0.0) / (SEUIL1 - 0.0))
        } else if m < SEUIL2 {
            mix(dark, yellow, (m - SEUIL1) / (SEUIL2 - SEUIL1))
        } else if m < SEUIL3 {
            mix(yellow, orange, (m - SEUIL2) / (SEUIL3 - SEUIL2))
        } else {
            mix(orange, red, (m - SEUIL3) / (1.0 - SEUIL3))
        }
    }

    return palette;
}

pub fn colormap_lookup(m: f64) -> [u8; 3] {
    let grey = [180, 212, 229];
    let dark = [40, 48, 64]; //SEUIL1
    let yellow = [192, 192, 18]; //SEUIL2
    let orange = [192, 25, 25]; //SEUIL3
    let red = [64, 0, 0];

    //if m > 0.0 {println!("{}",m)};

    if m < SEUIL1 {
        mix(grey, dark, (m - 0.0) / (SEUIL1 - 0.0))
    } else if m < SEUIL2 {
        mix(dark, yellow, (m - SEUIL1) / (SEUIL2 - SEUIL1))
    } else if m < SEUIL3 {
        mix(yellow, orange, (m - SEUIL2) / (SEUIL3 - SEUIL2))
    } else {
        mix(orange, red, (m - SEUIL3) / (1.0 - SEUIL3))
    }

    // old lokup into pregen palette, not really faster
    // mix(palette[i], palette[i+1], mu - i as f64)
}

pub fn julia_color(m: f64) -> [u8; 3] {
    let grey = [180, 212, 229];
    let dark = [40, 48, 64]; //SEUIL1
    let yellow = [192, 192, 18]; //SEUIL2
    let orange = [192, 25, 25]; //SEUIL3
    let red = [64, 0, 0];

    // dark
    let s1 = 0.50;
    // yellow
    let s2 = 0.70;
    // bright red
    let s3 = 0.80;

    if m < s1 {
        mix(grey, dark, (m - 0.0) / (s1 - 0.0))
    } else if m < s2 {
        mix(dark, yellow, (m - s1) / (s2 - s1))
    } else if m < s3 {
        mix(yellow, orange, (m - s2) / (s3 - s2))
    } else {
        mix(orange, red, (m - s3) / (1.0 - s3))
    }
}

pub fn colormap_k(m: f64) -> [u8; 3] {
    let grey = [180, 212, 229];
    let dark = [40, 48, 64]; //SEUIL1
    let yellow = [192, 192, 18]; //SEUIL2
    let orange = [192, 25, 25]; //SEUIL3
    let red = [64, 0, 0];

    // dark
    let s1 = 0.3;
    // yellow
    let s2 = 0.4;
    // bright red
    let s3 = 0.5;

    if m < s1 {
        mix(grey, dark, (m - 0.0) / (s1 - 0.0))
    } else if m < s2 {
        mix(dark, yellow, (m - s1) / (s2 - s1))
    } else if m < s3 {
        mix(yellow, orange, (m - s2) / (s3 - s2))
    } else {
        mix(orange, red, (m - s3) / (1.0 - s3))
    }
}
