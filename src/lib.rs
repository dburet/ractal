mod colorsfn;

#[macro_use]
extern crate lazy_static;

use anyhow::Result;
use image::ExtendedColorType::Rgba8;
use num::traits::Pow;
//use env_logger;
use image;
use log;
//use log::debug;
use num::complex::Complex64;
use num::Complex;
use pixels::{Pixels, SurfaceTexture};
use rayon::prelude::*;
use std::env;
use std::f64::consts::PI;
use std::fmt;
use std::time::Instant;
//use std::fs::File;
use colorsfn::{colormap_k, colormap_lookup, julia_color, mix};
use winit::{
    dpi::PhysicalSize,
    event::*,
    event_loop::{ControlFlow, EventLoop},
    //window::WindowBuilder,
    keyboard::*,
};
//use winit_input_helper::WinitInputHelper;

// square of escape circle
const R: f64 = 4.0;
//max iterations before stating that z is out of mandelbrot set
const MAX_ITER: usize = 255;
// zoom "step"
const ZOOM_FACTOR: f64 = 1.5;
// height factor of the incoming light
const H2: f64 = 1.5;
// incoming direction of light
const ANGLE: f64 = 45.0;
// SST - super sample level
const SST: usize = 200;

lazy_static! {
    static ref V: Complex64 = Complex::exp(Complex::new(0.0f64, 1.0f64) * ANGLE * 2.0 * PI / 360.0);
}

/// https://iquilezles.org/articles/distancefractals/

///
fn calc_distance(mut z: Complex<f64>, limit: usize) -> f64 {
    let c = z;
    let mut d2 = 2.0;
    let mut m2 = z.norm_sqr();

    for _i in 0..limit {
        d2 *= 4.0 * m2;
        z = z * z + c;
        m2 = z.norm_sqr();

        if m2 > 10_000_000_000.0 {
            return 0.5 * m2.ln() * (m2 / d2).sqrt();
        }
    }
    0.0
}

fn normal(c: Complex64) -> Complex64 {
    let eps = 0.000_000_000_1;

    let n = Complex::new(
        calc_distance(c + Complex { re: eps, im: 0.0 }, MAX_ITER)
            - calc_distance(c + Complex { re: -eps, im: 0.0 }, MAX_ITER),
        calc_distance(c + Complex { re: 0.0, im: eps }, MAX_ITER)
            - calc_distance(c + Complex { re: 0.0, im: -eps }, MAX_ITER),
    );
    n / n.norm()
}

fn render_mandelbrot_distance(
    img_buf: &mut [u8],
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
) {
    assert_eq!(img_buf.len(), bounds.0 * bounds.1 * 4);

    let v = (Complex64::new(0.0, 1.0) * 2.0 * std::f64::consts::PI * H2 / 360.0).exp();
    for row in 0..bounds.1 {
        for column in 0..bounds.0 {
            let point = pixel_to_point(bounds, (column, row), upper_left, lower_right);

            let u = normal(point);

            // light
            let mut t = (u * v).re + H2;
            t = t / (1.0 + H2);
            let mut e = [0, 0, 0x80];
            if t > 0.0 {
                e = mix([0, 0, 0], [255, 255, 255], t);
            }

            let offset = (row * bounds.0 + column) * 4;
            img_buf[offset] = e[0]; //r
            img_buf[offset + 1] = e[1]; //g
            img_buf[offset + 2] = e[2]; //b
            img_buf[offset + 3] = 0xff; //a
        }
    }
}

/// Render a rectangle of the Mandelbrot set into a buffer of pixels.
///
/// The `bounds` argument gives the width and height of the buffer `pixels`,
/// which holds one color  pixel per  3 bytes. The `upper_left` and `lower_right`
/// arguments specify points on the complex plane corresponding to the upper-
/// left and lower-right corners of the pixel buffer.
fn render_mandelbrot_color(
    img_buf: &mut [u8],
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
) {
    let ss = (lower_right.re - upper_left.re) / bounds.0 as f64 / 4.0;

    assert_eq!(img_buf.len(), bounds.0 * bounds.1 * 4);
    //let mut maxmu: f64 = 0.0;
    for row in 0..bounds.1 {
        for column in 0..bounds.0 {
            // convert pixel postion to complex plane postion

            let point = pixel_to_point(bounds, (column, row), upper_left, lower_right);

            // supersampling attempt
            let c1 = point + Complex::new(-ss, ss);
            let c2 = point + Complex::new(-ss, -ss);
            let c3 = point + Complex::new(ss, ss);
            let c4 = point + Complex::new(ss, -ss);
            let cs = vec![c1, c2, c3, c4];
            let mut e = [0, 0, 0];
            let mut sse = [0 as u16, 0 as u16, 0 as u16];
            let mut found = 4;
            for sub in cs {
                e = match escape_time(sub, MAX_ITER) {
                    None => [0, 0, 0],
                    Some((i, z)) => {
                        // https://iquilezles.org/articles/msetsmooth/[Inigo Quilez's article on smooth iteration count]
                        let modulus = z.norm();
                        let mu = i as f64 - modulus.ln().ln() / 2.0_f64.ln();

                        colormap_lookup(mu / MAX_ITER as f64)
                    }
                };
                sse[0] += e[0] as u16;
                sse[1] += e[1] as u16;
                sse[2] += e[2] as u16;
            }

            if found != 0 {
                e[0] = (sse[0] / found) as u8; //r
                e[1] = (sse[1] / found) as u8; //g
                e[2] = (sse[2] / found) as u8; //b
            } else {
                e = [0, 0, 0];
            };

            let offset = (row * bounds.0 + column) * 4;
            img_buf[offset] = e[0]; //r
            img_buf[offset + 1] = e[1]; //g
            img_buf[offset + 2] = e[2]; //b
            img_buf[offset + 3] = 0xff; //a
        }
    }
    //log::debug!("maxmu={}",maxmu);
}

/// Try to determine if `c` is in the Mandelbrot set, using at most `limit`
/// iterations to decide.
///
/// If `c` is not a member, return `Some(i)`, where `i` is the number of
/// iterations it took for `c` to leave the circle of radius two centered on the
/// origin. If `c` seems to be a member (more precisely, if we reached the
/// iteration limit without being able to prove that `c` is not a member),
/// return `None`.
fn escape_time(c: Complex<f64>, limit: usize) -> Option<(usize, Complex<f64>)> {
    let mut z = Complex::new(0.0, 0.0);

    for i in 0..limit {
        if z.norm_sqr() > R * R {
            z = z * z + c; // a couple of extra iterations helps
            z = z * z + c; // decrease the size of the error term.
            return Some((i + 2, z));
        }
        z = z * z + c;
    }
    None
}

///
/// open a windows, r'ender mandelbrot, event loop
pub fn run(
    filename: String,
    fractal_type: String,
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
    c0: Complex64,
) -> Result<()> {
    // a new world...
    let mut world = World::new(
        fractal_type,
        bounds.0,
        bounds.1,
        upper_left,
        lower_right,
        c0,
    );
    world.update();

    log::debug!("window init...");

    //let mut input = WinitInputHelper::new();
    //let event_loop = EventLoop::with_user_event()?;
    let event_loop = EventLoop::new().unwrap();
    let default_size = PhysicalSize::new(world.bounds.0 as f64, world.bounds.1 as f64);

    let window = winit::window::WindowBuilder::new()
        //.with_visible(false)
        .with_title(format!(
            "{} {}",
            env!("CARGO_PKG_NAME"),
            env!("CARGO_PKG_VERSION")
        ))
        .with_inner_size(default_size)
        .build(&event_loop)
        .unwrap();

    let size = window.inner_size();
    let surface_texture = SurfaceTexture::new(size.width, size.height, &window);

    let mut pixels = Pixels::new(size.width, size.height, surface_texture).unwrap();

    // dump starting parameters
    log::debug!("initial world {:?}", world);
    log::debug!("windows.inner_size: {:?}", window.inner_size());
    log::debug!("windows.outer_size: {:?}", window.outer_size());
    log::debug!("windows.inner_position: {:?}", window.inner_position());
    log::debug!("windows.outer_position: {:?}", window.outer_position());

    log::debug!("starting event loop...");

    // Variables pour stocker la position de la souris
    let mut mouse_x = 0.0;
    let mut mouse_y = 0.0;

    event_loop.set_control_flow(ControlFlow::Wait);
    let _ = event_loop.run(move |event, _| {
        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => {
                    // Fermer la fenêtre lorsque l'utilisateur demande à la fermer
                    // event_loop.set_control_flow(ControlFlow::);
                    return;
                }
                WindowEvent::RedrawRequested => {
                    log::debug!("Event::RedrawRequested received");
                    world.draw(pixels.frame_mut());
                    let _ = pixels.render();
                }

                WindowEvent::Resized(_size) => {
                    // Redimensionner l'image lorsque l'utilisateur redimensionne la fenêtre
                    let width = window.inner_size().width ;
                    let heigth = window.inner_size().height ;
                    let _ = pixels.resize_buffer(width, heigth);
                    let _ = pixels.resize_surface(width, heigth);
                    world.resize( width as usize, heigth as usize);
                    window.request_redraw();
                }

                WindowEvent::KeyboardInput { event, .. } => {
                    if !event.state.is_pressed() {return; }
                    let key = event.logical_key;
                    log::debug!("key press:{:?}", key);
                    match key {
                        Key::Named(NamedKey::PageUp) => {
                            log::debug!("Touche PageUp appuyée !");
                            world.scale_complex(ZOOM_FACTOR);
                            world.update();
                            window.request_redraw();
                            return;
                        }
                        Key::Named(NamedKey::PageDown) => {
                            log::debug!("Touche PageDown appuyée !");
                            world.scale_complex(1.0 / ZOOM_FACTOR);
                            world.update();
                            window.request_redraw();
                            return;
                        }           
                        Key::Named(NamedKey::Enter)  => {
                            log::debug!("Save...");
                            write_image(&filename, &world.img_buf, world.bounds)
                                .expect("save image failed.");
                            return;
                        }
                        Key::Named(NamedKey::Escape) => {
                            log::debug!("Touche ESC appuyée !");
                            std::process::exit(0);
                        }
                        Key::Named(NamedKey::Space) => {
                            log::debug!("Touche SPACE appuyée !");
                            println!("ractal -f yourfile.png -s {:?}x{:?} -u \'({:?},{:?})\' -l \'({:?},{:?})\' -c '({:?},{:?})' ",
                                world.bounds.0,world.bounds.1,
                                world.upper_left.re,world.upper_left.im,
                                world.lower_right.re, world.lower_right.im,
                                world.c0.re, world.c0.im);
                            return;
                        }
                        _ => (),
                    }
                }
                WindowEvent::MouseInput {
                    state,
                    button,
                    device_id:_,
                } => {
                    if state == ElementState::Pressed && button == MouseButton::Left {
                        // Gérer le clic gauche de la souris
                        log::debug!(
                            "Clic gauche de la souris, position: {:?} {:?}",
                            mouse_x,
                            mouse_y
                        );
                        world.center(mouse_x as usize, mouse_y as usize);
                        window.request_redraw();
                    }
                }
                WindowEvent::CursorMoved { position, .. } => {
                    mouse_x = position.x;
                    mouse_y = position.y;
                }
                _ => (),
            },
            _ => (),
        }
    });

    Ok(())
}

pub fn save_fractal(
    filename: String,
    fractal_type: String,
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
    c0: Complex64,
) -> Result<()> {
    // a new world...
    let mut world = World::new(
        fractal_type,
        bounds.0,
        bounds.1,
        upper_left,
        lower_right,
        c0,
    );
    world.update();

    write_image(&filename, &world.img_buf, world.bounds)
                                .expect("save image failed.");
    
    Ok(())
}

// TODO: move world and renderer to a module

struct World {
    upper_left: Complex64, //coordinatates of upper left corner in the complex plane
    lower_right: Complex64, //coordinatates of lower right corner in the complex plane
    bounds: (usize, usize),
    c0: Complex64,
    img_buf: Vec<u8>,
    fractal_type: String, //_renderer: Box<dyn Fn( &mut [u8],(usize, usize),Complex64, Complex64)>,
}

impl fmt::Debug for World {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("World")
            .field("upper_left", &self.upper_left)
            .field("lower_right", &self.lower_right)
            .field("bounds", &self.bounds)
            .finish()
    }
}

impl World {
    ///
    fn new(
        ftype: String,
        width: usize,
        height: usize,
        ul: Complex64,
        lr: Complex64,
        c0: Complex64,
    ) -> Self {
        Self {
            upper_left: ul,
            lower_right: lr,
            c0: c0,
            bounds: (width, height),
            img_buf: vec![(0)],
            fractal_type: ftype,
        }
    }

    fn center(&mut self, x: usize, y: usize) {
        /*  upper_left: Complex64,  //coordinatates of upper left corner in the complex plane
        lower_right: Complex64, //coordinatates of lower right corner in the complex plane
        bounds: (usize, usize),
        img_buf: Vec<u8>,
        palette: Vec<[u8; 3]>,
        */
        let new_center = pixel_to_point(self.bounds, (x, y), self.upper_left, self.lower_right);
        let w = self.lower_right.re - self.upper_left.re;
        let h = -self.lower_right.im + self.upper_left.im;
        self.upper_left.re = new_center.re - w / 2.0;
        self.lower_right.re = new_center.re + w / 2.0;
        self.upper_left.im = new_center.im + h / 2.0;
        self.lower_right.im = new_center.im - h / 2.0;
        self.update()
    }

    /// Update the `World` internal state
    fn update(&mut self) {
        log::debug!("--> World.update");
        let now = Instant::now();
        let mut img_buf = vec![0 as u8; self.bounds.0 * self.bounds.1 * 4];
        // calc img_buf
        let bands: Vec<(usize, &mut [u8])> =
            img_buf.chunks_mut(self.bounds.0 * 4).enumerate().collect();

        // render bands in parallel
        bands.into_par_iter().for_each(|(i, band)| {
            let top = i;
            let (w, _h) = self.bounds;
            let band_bounds = (w, 1);
            let band_upper_left =
                pixel_to_point(self.bounds, (0, top), self.upper_left, self.lower_right);
            let band_lower_right = pixel_to_point(
                self.bounds,
                (self.bounds.0, top + 1),
                self.upper_left,
                self.lower_right,
            );

            // you can change here the renderer
            // * render_mandelbrot_color is a classic color=f(nb iteration)
            // render_mandelbrot_k_color is a classic color=f(nb iteration), with Z^k
            // * render_mandelbrot_distance use a normal map
            // * render_julia for a julia set
            // TODO: choose render with command line
            match self.fractal_type.as_str() {
                "julia" => render_julia(
                    self.c0,
                    band,
                    band_bounds,
                    band_upper_left,
                    band_lower_right,
                ),
                "mandelbrot" => {
                    render_mandelbrot_color(band, band_bounds, band_upper_left, band_lower_right)
                }
                "mandelbrot_normal" => {
                    render_mandelbrot_distance(band, band_bounds, band_upper_left, band_lower_right)
                }
                //"mandelbrot_k" => render_mandelbrot_k_color(k, band, band_bounds, band_upper_left, band_lower_right),
                _ => (),
            }

            // render_mandelbrot_distance( band, band_bounds, band_upper_left, band_lower_right);
            // render_mandelbrot_k_color(7.0, band, band_bounds, band_upper_left, band_lower_right);
        });
        self.img_buf = img_buf;
        log::debug!("<-- World.update in {:.2?}", now.elapsed());
    }

    /// Draw the `World` state to the frame buffer.
    ///
    /// Assumes the default texture format: `wgpu::TextureFormat::Rgba8UnormSrgb`
    fn draw(&self, screen: &mut [u8]) {
        log::debug!(
            "--> World.draw - screen : {:?}  vs  img_buf : {:?} ",
            screen.len(),
            self.img_buf.len()
        );
        assert_eq!(self.img_buf.len(), screen.len());

        for (color, pix) in self
            .img_buf
            .chunks_exact(32)
            .zip(screen.chunks_exact_mut(32))
        {
            pix.copy_from_slice(color);
        }
        log::debug!("<-- World.draw");
    }

    fn scale_complex(&mut self, factor: f64) {
        let center = (self.upper_left + self.lower_right) / 2.0;
        self.lower_right = factor * (self.lower_right - center) + center;
        self.upper_left = factor * (self.upper_left - center) + center;

        self.update();
    }

    fn resize(&mut self, sx: usize, sy: usize) {
        log::debug!("--> resize : {:?}", self);
        self.lower_right = pixel_to_point(self.bounds, (sx, sy), self.upper_left, self.lower_right);

        self.bounds = (sx, sy);

        log::debug!("<-- resize : {:?}", self);

        self.update();
    }
}

/// Write the buffer `img_buf`, whose dimensions are given by `bounds`, to the
/// file named `filename`.
fn write_image(
    filename: &String,
    img_buf: &[u8],
    bounds: (usize, usize),
) -> image::ImageResult<()> {
    image::save_buffer(filename, img_buf, bounds.0 as u32, bounds.1 as u32, Rgba8)
}

/// Given the row and column of a pixel in the output image, return the
/// corresponding point on the complex plane.
///
/// `bounds` is a pair giving the width and height of the image in pixels.
/// `pixel` is a (column, row) pair indicating a particular pixel in that image.
/// The `upper_left` and `lower_right` parameters are points on the complex
/// plane designating the area our image covers.
fn pixel_to_point(
    bounds: (usize, usize),
    pixel: (usize, usize),
    upper_left: Complex<f64>,
    lower_right: Complex<f64>,
) -> Complex<f64> {
    let (width, height) = (
        lower_right.re - upper_left.re,
        upper_left.im - lower_right.im,
    );
    Complex {
        re: upper_left.re + pixel.0 as f64 * width / bounds.0 as f64,
        im: upper_left.im - pixel.1 as f64 * height / bounds.1 as f64,
        // Why subtraction here? pixel.1 increases as we go down,
        // but the imaginary component increases as we go up.
    }
}

#[test]
fn test_pixel_to_point() {
    assert_eq!(
        pixel_to_point(
            (100, 100),
            (25, 75),
            Complex { re: -1.0, im: 1.0 },
            Complex { re: 1.0, im: -1.0 }
        ),
        Complex { re: -0.5, im: -0.5 }
    );
}

/// Try to determine if `c` is in the Mandelbrot set, using at most `limit`
/// iterations to decide.
///
/// If `z0` is not a member, return `Some(i/limit)`, where `i` is the number of
/// iterations it took for `z` to leave the circle of radius two centered on the
/// origin. If `z` seems to be a member (more precisely, if we reached the
/// iteration limit without being able to prove that `z` is not a member),
/// return `None`.

/// Render a rectangle of a julia set into a buffer of pixels.
///
/// The `bounds` argument gives the width and height of the buffer `pixels`,
/// which holds one color  pixel per  3 bytes. The `upper_left` and `lower_right`
/// arguments specify points on the complex plane corresponding to the upper-
/// left and lower-right corners of the pixel buffer.
fn render_julia(
    c: Complex64,
    img_buf: &mut [u8],
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
) {
    fn julia(
        c: Complex<f64>,
        z0: Complex<f64>,
        limit: usize,
    ) -> Option<(f64, Complex<f64>, Complex<f64>)> {
        let mut z = z0;
        let mut dc = Complex::new(1.0, 0.0);
        //println!("c: {} ;  Z0: {} ; z: {} ",c,z0,z);
        for i in 0..limit * 2 {
            if z.norm_sqr() > R * R {
                z = z * z + c; // a couple of extra iterations helps
                z = z * z + c; // decrease the size of the error term.
                let modulus = z.norm();
                let mu = (i as f64 - modulus.ln().ln() / 2.0_f64.ln()) / MAX_ITER as f64;
                return Some((mu, z, dc));
            }
            z = z * z + c;
            // will be used for distance estimation in the future
            dc = dc * 2.0 * z + 1.0;
        }
        None
    }

    assert_eq!(img_buf.len(), bounds.0 * bounds.1 * 4);
    //let mut maxmu: f64 = 0.0;
    for row in 0..bounds.1 {
        for column in 0..bounds.0 {
            // convert pixel postion to complex plane postion
            let point = pixel_to_point(bounds, (column, row), upper_left, lower_right);
            let offset = (row * bounds.0 + column) * 4;
            log::debug!(
                "calling julia with point {} - bounds: {},{} - ul:{}  - lr:{}  - c:{}",
                point,
                bounds.0,
                bounds.1,
                upper_left,
                lower_right,
                c
            );
            let e = match julia(c, point, MAX_ITER) {
                None => [0, 0, 0],
                Some((e_time, _z, _dc)) => {
                    // simple coloring (color index in palette = nb iter)
                    //palette[i*255/MAX_ITER]

                    // http://linas.org/art-gallery/escape/escape.html
                    // continuous iteration
                    //let modulus = z.norm();
                    //let mu = i as f64 - modulus.ln().ln() / 2.0_f64.ln();

                    //if mu> maxmu {maxmu=mu;};
                    //palette[mu as usize];
                    julia_color(e_time)

                    // supersampling if near border
                    /*if i > SST {palette[i*255/MAX_ITER]}
                    else  {
                        //super sampling
                        ....
                    }*/

                    //color_normal_map(z, dc)
                }
            };
            img_buf[offset] = e[0]; //r
            img_buf[offset + 1] = e[1]; //g
            img_buf[offset + 2] = e[2]; //b
            img_buf[offset + 3] = 0xff; //a
        }
    }
    //log::debug!("maxmu={}",maxmu);
}

/// Render a rectangle of the Mandelbrot set into a buffer of pixels.
///
/// The `bounds` argument gives the width and height of the buffer `pixels`,
/// which holds one color  pixel per  3 bytes. The `upper_left` and `lower_right`
/// arguments specify points on the complex plane corresponding to the upper-
/// left and lower-right corners of the pixel buffer.
fn render_mandelbrot_k_color(
    k: f64,
    img_buf: &mut [u8],
    bounds: (usize, usize),
    upper_left: Complex64,
    lower_right: Complex64,
) {
    assert_eq!(img_buf.len(), bounds.0 * bounds.1 * 4);

    let max_iter = 100;
    let threshold = 64.0;

    let b: f64 = 2.0_f64.pow(1.0 / (k - 1.0));
    let b_ln = b.ln();

    for row in 0..bounds.1 {
        for column in 0..bounds.0 {
            // convert pixel postion to complex plane postion
            let point = pixel_to_point(bounds, (column, row), upper_left, lower_right);
            let offset = (row * bounds.0 + column) * 4;
            let e = match escape_time_k(point, k, max_iter, threshold) {
                None => [0, 0, 0],
                Some((i, z)) => {
                    // continuous iteration - https://iquilezles.org/articles/msetsmooth/
                    let modulus = z.norm();

                    let mu = i as f64 - (modulus.ln() / threshold.ln()).ln() / k.ln();

                    colormap_k(mu / max_iter as f64)

                    // supersampling if near border
                    /*if i > SST {palette[i*255/MAX_ITER]}
                    else  {
                        //super sampling
                        ....
                    }*/

                    //color_normal_map(z, dc)
                }
            };
            img_buf[offset] = e[0]; //r
            img_buf[offset + 1] = e[1]; //g
            img_buf[offset + 2] = e[2]; //b
            img_buf[offset + 3] = 0xff; //a
        }
    }
}

/// Try to determine if `c` is in the Mandelbrot7 set, using at most `limit`
/// iterations to decide.
///
/// If `c` is not a member, return `Some(i)`, where `i` is the number of
/// iterations it took for `c` to leave the circle of radius two centered on the
/// origin. If `c` seems to be a member (more precisely, if we reached the
/// iteration limit without being able to prove that `c` is not a member),
/// return `None`.
fn escape_time_k(
    c: Complex<f64>,
    k: f64,
    limit: usize,
    square_orbit: f64,
) -> Option<(usize, Complex<f64>)> {
    let mut z = Complex::new(0.0, 0.0);

    for i in 0..limit {
        if z.norm_sqr() > square_orbit {
            z = z.pow(k) + c;
            z = z.pow(k) + c;
            return Some((i + 2, z));
        }
        z = z.pow(k) + c;
    }
    None
}
