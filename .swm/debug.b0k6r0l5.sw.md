---
title: DEBUG
---

We use the env_logger crate, c.f. [env_logger - Rust (docs.rs)](https://docs.rs/env_logger/latest/env_logger/)

to enable debug, just set the RUST_LOG env var

format is: RUST_LOG=\[target\]\[=\]\[level\]\[,...\]

short: RUST_LOG=debug\
\
log levels are:

- `warn`

- `error`(default)

- `info`

- `debug`

- `trace`

- `off` (pseudo level to disable all logging for the target)

&nbsp;

&nbsp;

<SwmMeta version="3.0.0" repo-id="Z2l0bGFiJTNBJTNBcmFjdGFsJTNBJTNBZGJ1cmV0" repo-name="ractal"><sup>Powered by [Swimm](https://app.swimm.io/)</sup></SwmMeta>
